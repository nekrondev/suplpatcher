### Sailfish OS Xperia devices A-GPS SUPL Patcher ###

```text                                 
   _____ __  ______  __       ____        __       __             
  / ___// / / / __ \/ /      / __ \____ _/ /______/ /_  ___  _____
  \__ \/ / / / /_/ / /      / /_/ / __ `/ __/ ___/ __ \/ _ \/ ___/
 ___/ / /_/ / ____/ /___   / ____/ /_/ / /_/ /__/ / / /  __/ /    
/____/\____/_/   /_____/  /_/    \__,_/\__/\___/_/ /_/\___/_/     
                                                                  
```                                                                  

#### What's this all about? ####
This is a small command line tool to fix your long lasting A-GPS issues with official supported Xperia X devices.

Use it at our own risk.
```
[root@Xperia10II-DualSIM defaultuser]# ./suplpatch --help
Usage of ./suplpatch:
  -aosp int
        AOSP base version (6, 7, 8, 9, 10 or 11)
  -disable
        disable SUPL TLS transport in modem
  -enable
        enable SUPL TLS transport in modem
  -info
        print modem information
  -remove
        remove Google and Let's Encrypt root certs from modem
  -supl int
        set SUPL default version in modem (1 or 2, note: 2 is mandatory for 4G)
  -upload
        upload Google and Let's Encrypt root certs to modem
  -version
        show version
```

#### Who should use it? ####
1. You should be familar with Linux shell and have developer mode enabled on your device
2. Your device suffers from delayed GPS fixes and you want to speed things up
3. Your are aware of Google SUPL service and decreased privacy

#### Requirements for working A-GPS solution ####
A-GPS / SUPL support is a modem-internal process. Therefor the modem must

1. have a valid SIM card in your device
2. have GPRS / data connection setup and activated, i.e. you can connect to the internet from your mobile *without* using wifi
3. have location service activated and high precision option selected (not exclusive device more nor custom settings)
4. have positioning package from harbour installed for the region you are living
5. nope, it's not working on wifi-only connections, go buy a mobile subscription
6. __reboot__ your modem (phone) after initial patching

#### Preparation ####
Install binary

1. copy `suplpatch` binary to your device (scp or use terminal app)
2. `devel-su`
3. `chmod +x suplpatch`

Optional: Backup your modem settings

4. `ls -l /dev/block/platform/soc.0/by-name/ | grep modemst`

Find the mmcblk devices with `modemst1` and `modemst2`.

Example:
```
lrwxrwxrwx    1 root     root            22 Jan 16 16:49 modemst1 -> ../../../../mmcblk0p14
lrwxrwxrwx    1 root     root            22 Jan 16 16:49 modemst2 -> ../../../../mmcblk0p15
```

5. `dd if=/dev/mmcblk0p14 of=modemst1.bin`
6. `dd if=/dev/mmcblk0p15 of=modemst2.bin`

#### Option 1: Disable SUPL TLS transport ####
This is the easiest way and no certificates will be uploaded to modem.
However after this all SUPL requests will be send unencrypted over the internet to Google or other unencrypted SUPL services. 

You can trigger TLS deactivation using the `--disable` flag.

```
[root@Xperia10II-DualSIM defaultuser]# ./suplpatch --aosp 10 --disable

   ______  _____  __     ___       __      __
  / __/ / / / _ \/ /    / _ \___ _/ /_____/ /  ___ ____
 _\ \/ /_/ / ___/ /__  / ___/ _ `/ __/ __/ _ \/ -_) __/
/___/\____/_/  /____/ /_/   \_,_/\__/\__/_//_/\__/_/

SailfishOS Xperia X devices A-GPS SUPL Patcher v0.0.2.0 (git: d41f150, build: 02.07.2022 13:59)

2022/07/02 14:02:06 ------------------------------------------------------------------------------------------
2022/07/02 14:02:06 SailfishOS Xperia X devices A-GPS SUPL Patcher v0.0.2.0 (git: d41f150, build: 02.07.2022 13:59)
2022/07/02 14:02:06 Command line './suplpatch' launched with the following flags ...
2022/07/02 14:02:06 --aosp=10
2022/07/02 14:02:06 --disable=true
2022/07/02 14:02:06 --enable=false
2022/07/02 14:02:06 --info=false
2022/07/02 14:02:06 --remove=false
2022/07/02 14:02:06 --supl=0
2022/07/02 14:02:06 --upload=false
2022/07/02 14:02:06 --version=false
2022/07/02 14:02:06 Executable launched as user 'root@Xperia10II-DualSIM'.
2022/07/02 14:02:06 Secure SUPL mode successfully deactivated.
2022/07/02 14:02:06 Bye.
```

This setting can be reverted using the `--enable` flag to turn on SUPL TLS again.
Note: For a working TLS protected SUPL request you need updated root certificates in your modem (see option 2).

Now that you disabled SUPL TLS transport you must change your `gps.conf` file.
It can be located at `/etc/gps.conf` (Xperia X, ...), `/system/vendor/etc/gps.conf` (Xperia 10 II) or `/vendor/etc/gps.conf` (Xperia 10 III).

```
# FOR SUPL SUPPORT, set the following
SUPL_HOST=supl.google.com
SUPL_PORT=7276
```
Change `SUPL_HOST` to `supl.google.com` and `SUPL_PORT` to `7276`.
This is the unencrypted SUPL service provided by Google (port 7275 uses TLS).

Reboot your device to clear previously cached volatile ephemeris data in modem.
If you changed your location settings to `high precision` after injection of the SUPL host your modem will fetch ephemeris data from the unencrypted Google endpoint and you should get an A-GPS fix within a few seconds or even faster.

Since SailfishOS might still try to connect to legacy MLS you should also add
```
127.0.0.1       location.services.mozilla.com
```
into your `/etc/hosts` file to make things a tick faster.

#### Option 2: Upload additional ROOT certificates ####
Well, if only Google should know your SUPL request data you should use SUPL TLS transport which is the default modem setting without patching.

However to get this working you need a valid root certificate injected into modem via QMI but missing libhybris bindings and libgeoclue parser support for `gps.conf` file will not allow this at the moment so you have to upload the required certificates to modem manually.

This is triggered via the `--upload` flag.

```
[root@Xperia10II-DualSIM defaultuser]# ./suplpatch --aosp 10 --upload 

   ______  _____  __     ___       __      __
  / __/ / / / _ \/ /    / _ \___ _/ /_____/ /  ___ ____
 _\ \/ /_/ / ___/ /__  / ___/ _ `/ __/ __/ _ \/ -_) __/
/___/\____/_/  /____/ /_/   \_,_/\__/\__/_//_/\__/_/

SailfishOS Xperia X devices A-GPS SUPL Patcher v0.0.2.0 (git: d41f150, build: 02.07.2022 13:59)

2022/07/02 14:03:44 ------------------------------------------------------------------------------------------
2022/07/02 14:03:44 SailfishOS Xperia X devices A-GPS SUPL Patcher v0.0.2.0 (git: d41f150, build: 02.07.2022 13:59)
2022/07/02 14:03:44 Command line './suplpatch' launched with the following flags ...
2022/07/02 14:03:44 --aosp=10
2022/07/02 14:03:44 --disable=false
2022/07/02 14:03:44 --enable=false
2022/07/02 14:03:44 --info=false
2022/07/02 14:03:44 --remove=false
2022/07/02 14:03:44 --supl=0
2022/07/02 14:03:44 --upload=true
2022/07/02 14:03:44 --version=false
2022/07/02 14:03:44 Executable launched as user 'root@Xperia10II-DualSIM'.
2022/07/02 14:03:44 CyberTrust root certificate for Google SUPL uploaded to modem successfully.
2022/07/02 14:03:44 CyberTrust subscription 1 root certificate for Google SUPL uploaded to modem successfully.
2022/07/02 14:03:44 ISRGRootX1 root certificate for Let's Encrypt based SUPLs uploaded to modem successfully.
2022/07/02 14:03:44 ISRGRootX1 subscription 1 root certificate for Let's Encrypt based SUPLs uploaded to modem successfully.
2022/07/02 14:03:44 Bye.
```
Four new root certificates will be uploaded to your modem (SuplRootCert will **not** be replaced or modified) supporting SIM1 and SIM2 subscriptions.

You can revert this (remove the certificates from modem) using the `--remove` flag.

The uploaded certificates are:
 * CyberTrust Root CA used by Google SUPL service (https://pki.goog/roots.pem)
 * ISRGRootX1 used by Let's Encrypt service (https://letsencrypt.org/certs/isrgrootx1.pem)

 This gives you the option to host your own TLS encrypted SUPL proxy using a Let's Encrypt signed certificate or otherwise use the Google SUPL TLS service directly.

Now that you enabled SUPL TLS transport you must change your `gps.conf` file.
It can be located at `/etc/gps.conf` (Xperia X, ...), `/system/vendor/etc/gps.conf` (Xperia 10 II) or `/vendor/etc/gps.conf` (Xperia 10 III).

```
# FOR SUPL SUPPORT, set the following
SUPL_HOST=supl.google.com
SUPL_PORT=7275
```
Change `SUPL_HOST` to `supl.google.com` and `SUPL_PORT` to `7275`.
This is the encrypted SUPL service provided by Google (port 7276 is unencrpyted transport).

Reboot your device to clear previously cached volatile ephimeris data in modem.
If you changed your location settings to `high precision` after injection of the SUPL host your modem will fetch ephimeris data from the unencrypted Google endpoint and you should get an A-GPS fix within a few seconds or even faster.

Since SailfishOS might still try to connect to legacy MLS you should also add
```
127.0.0.1       location.services.mozilla.com
```
into your `/etc/hosts` file to make things a tick faster.

#### A Word On 4G SUPL Support ####
It is mandatory to use SUPLv2 on 4G connected devices. 
To change the SUPL version setting I found out that patching the modems SUPL register is not needed because Sony vendor blob is initializing the setting for you based on `gps.conf` entry.

For SUPLv2 support please add the following setting into the `gps.conf` file:

```
SUPL_VER=0x20000
```

#### It's not working for me ####
I've tested `suplpatch` with Xperia X, X10 Mark 2 and X10 Mark 3 and get an A-GPS fix in less than 10 seconds even if the device wasn't using GPS for many days and ephemeris data was expired.

Deactivation of SUPL TLS was verified by proxying requests from a public server to `supl.google.com` and examing the requests. No TLS hello package or other handshake overhead was send so TLS deactivation works as expected.

Last but not least: Did you reboot your phone after patching?

#### Pro-Tip ####
If you don't worry about your privacy you should simply disable SUPL TLS as TLS handshake uses more traffic than plain SUPL request/response itself.

#### FAQ ####
_Is this an official tool released by Jolla?_
- **NO** it's a community members attempt to help fixing your A-GPS problems

_What does `ERR: Unable to open '/dev/diag' device (ioctl DIAG_IOCTL_SWITCH_LOGGING fails (12)).` mean?_
- Please check your --aosp value as it must match the Sailfish OS AOSP base (this is NOT the Android base used in aliendalvik container!). E.g. Xperia 10 Mark 2 is AOSP 10, Xperia 10 Mark 3 is AOPS 11 where the intial Xperia X uses AOSP 6 as Android base.

_How do I ... on my phone?_
- If you are not familar with Linux shell or editing tools please wait for an official A-GPS solution by Jolla

_Can you add feature ... into the tool?_
- This is not a hacking tool for your device modem so don't expect to see VoLTE/VoWifi profile modifications or unlocking of locked radio bands ever

_Is this working for Jolla or Jolla C phones?_
- Sorry but it's sadly not working (modem security / diag driver issues)

_Is it working for 4G networks?_
- To get SUPL working with 4G networks please set the default modem SUPL version to v2 adding the `SUPL_VER` key into `gps.conf` file.

_I don't trust you_
- That's ok so please don't use this tool.

_What are you patching on my modem?_
- `suplpatcher` is _only_ modifying a single non-volatile setting to disable / enable secure SUPL transport (--disable , --enable flag) which can't be changed using the `gps.conf` file.
- `suplpatcher` is uploading four new root certificates to get secure SUPL working (--upload, --remove flag). The certificates are uploaded to modems internal embedded files system (EFS) and can be removed by using the `--remove` flag. It is currently unclear if vendor blob supports certificate injection or if this function must be implemented by SailfishOS.

_I've updated my SailfishOS phone, now what?_
- If you had already patched your secure SUPL settings in the modem you don't have to re-run `suplatcher`. SailfishOS system upgrade will not change internal NV / EFS modem settings.
- Alas `gps.conf` file must be modified again as system upgrade will overwrite this file with the stock one from Sonys AOSP repository

#### Changelog ####
v0.0.2.1
- Added FAQ entry about SailfishOS system upgrades

v0.0.2.0
- Added support for AOSP base 11 to get `suplpatcher` working with Xperia 10 Mark 3
- Added a new flag to modify SUPL modem default version (needed for 4G SUPL support)
- CyberTrust and ISG root certificates created for second subscription (SIM2)
- Updated `README` based on new lessons learned (removed `--supl` flag instructions as patching the SUPL version register is no longer needed)

